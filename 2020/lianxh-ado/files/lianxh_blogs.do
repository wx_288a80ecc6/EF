
/*
1. 做成一个 ado 程序，可以自动输出最终的 md 文档
2. 头部和尾部信息可以另设一个 txt 文档，append 到上述 md 文档中
3. 程序 2 lianxh keyword --> 列出

*-options
  - Date	输出的 md 文档中是否包含日期，若加上就包含日期
  - Catlink	推文分类是否包含超链接
  - NOTime	不输出按日期分类的推文列表
  - NOTOP	不附加头尾信息
  - Saving() 存储路径，默认为 D:\vsCode\推文-lianxh.cn\OK-lianxh主页\主页推文列表" 
*/


global path "D:\stata15\ado\personal\lianxh_Blogs"

global D "$path/data"
global out "$path/out"

cd $D

copy "https://www.lianxh.cn/blogs.html" "lianxh.txt", replace
copy "https://www.lianxh.cn/blogs.html" "lianxh.html", replace //便于查看代码
infix strL v 1-1000 using "lianxh.txt", clear
save "lxh_temp.dta", replace  // 原始网页数据



*------------------
*- 推文标题
*------------------

use "lxh_temp.dta", replace
* (?<="><a href=")(.*)(?:")

*-抽取推文链接  ok
local regex = `"(?<="><a href=")(.*)(?=")"' 
* 以 「"><a href="」开头，「"」结尾的字符串 
gen link = ustrregexs(0) if ustrregexm(v, `"`regex'"')
replace link = "https://www.lianxh.cn" + link if link!=""

*-抽取推文标题 

*local regex = `"(?<=h3.*html">)(.*)(?=</a></h3>)"'    //??????这里执行不了
*local regex = `"(?<=h3(.*)html">)(.*)(?=</a></h3>)"'  //也不行

local regex = `"(?<=html">)(.+)(?=</a></h3>)"'
gen title = ustrregexs(0) if ustrregexm(v, `"`regex'"')



*-抽取分类标签
*local regex = `"(\t{9}<span>)(\D.*)(?:</span>)"'  // vscode中可行
local regex = `"(<span>)(\D.*)(?:</span>)"'  
*-Note: 需要|<span>结果输出</span>|，而不需要 |<span>04/02</span>|, 所以，使用「\D」排除后者
gen cat = ustrregexs(2) if ustrregexm(v, `"`regex'"')
drop if ustrregexm(cat, "(data\[item\])")
*br if link!=""|cat!=""
keep if link!=""|cat!=""
replace cat = cat[_n+1] if cat==""
keep if title!=""

/*
*-抽取发表时间 (暂时放弃，2020/10/19 13:18)

local regex = `"(<span>)(\d.*)(?:</span>)"'  
*-Note: 需要 |<span>04/02</span>|
gen date = ustrregexs(2) if ustrregexm(v, `"`regex'"')
drop if ustrregexm(cat, "(data\[item\])")
*br if link!=""|cat!=""
keep if link!=""|cat!=""
replace cat = cat[_n+1] if cat==""
keep if title!=""
*/


*-剔除不需要的
drop v
drop if ustrregexm(title, "(data\[item\])")
format title %-60s
gen id = _n         // 推文发表时间序号
*gsort cat title

list 

save "lxh_title.dta", replace 



*-分类标签按顺序呈现
  use "lxh_temp.dta", replace
  
  local regex = `"(?<=/blogs/)\d+(?=\.html)"'
  gen catID = ustrregexs(0) if ustrregexm(v, `"`regex'"')  // 分类编号

  keep if catID != ""
  local regex = `"(?<=\s>).+(?=</a>)"'
  gen cat = ustrregexs(0) if ustrregexm(v, `"`regex'"')  // 分类名称
  drop v 
  destring catID, replace 
  save "catID.dta", replace  // 临时保存文件，随后与主文件合并 
  
  *-呈现顺序
    preserve
	  clear
	  input int catID	str30 cat	int cat123
                  44	专题课程	1
                  34	数据分享	2
                  31	论文写作	3
                  43	Stata新命令	4
                  16	Stata入门	8
                  17	Stata教程	9
                  18	计量专题	10
                  35	Stata资源	11
                  25	Stata数据处理	12
                  24	Stata绘图	13
                  26	Stata程序	14
                  22	结果输出	17
                  32	回归分析	21
                  20	面板数据	22
                  38	IV-GMM	24
                  39	倍分法DID	26
                  40	断点回归RDD	28
                  41	PSM-Matching	30
                  42	合成控制法	32
                  19	内生性-因果推断	43
                  21	交乘项-调节	45
                  28	时间序列	47
                  29	空间计量	49
                  27	Probit-Logit	51
                  36	文本分析-爬虫	61
                  37	Python-R-Matlab	63
                  45	风险管理	96
                  30	Markdown	97
                  23	工具软件	98
                  33	其它	99
       end
	   compress 
	   tostring catID, gen(catID_str)
	   *- https://www.lianxh.cn/blogs/20.html
	   local url "https://www.lianxh.cn/blogs/"
	   gen linkCat = "[" + cat + "](" + "`url'" + catID_str + ".html" + ")"
	   save "catSort.dta", replace  // 包含分类排序的数据
	restore 
    
*-合并数据
  use "catID.dta", clear 
  merge 1:1 catID using "catSort.dta", nogen 
  merge 1:m cat   using "lxh_title.dta", nogen 
  
  gen list  =   "- [" + title + "](" + link + ")"
 *gen list2 = "  - [" + title + "](" + link + ")"
  
  save "Final_data.dta", replace 
  
  
  
  
  
*---------------------
*- 输出 Markdown 文档
*---------------------

use "Final_data.dta", clear 

  global pp "D:\vsCode\推文-lianxh.cn\OK-lianxh主页\主页推文列表" 

  
*-v1: 按时间顺序排序
preserve    
  local date = subinstr("`c(current_date)'"," ","",3)
  insobs 4, before(1)  //增加一行观察值，以便写大标题
  replace id = -9 in 1
  replace id = -8 in 2
  replace id = -7 in 3
  replace id = -6 in 4
  
  replace list = "## 连享会 - 推文列表" if id==-9
  replace list = "> &emsp;     " if id==-8
  replace list = "> &#x231A; Update: ``date'` &emsp;  &#x2B55; [**按类别查看**](https://www.lianxh.cn/news/d4d5cd7220bc7.html)  " if id==-7
  replace list = "> &emsp;     " if id==-6
  
    sort id
	local date = subinstr("`c(current_date)'"," ","",3)
	*dis "`date'"
    export delimited list using "连享会主页_推文列表-时间_`date'.md", ///
           novar nolabel delimiter(tab) replace	 

    export delimited list using "$pp/连享会主页_推文列表-时间_`date'.md", ///
           novar nolabel delimiter(tab) replace	
restore 





*-v2: 分类排序

use "Final_data.dta", clear 

keep cat123 id list linkCat cat
format list linkCat %-20s

  sort cat123 id 
  gen id_temp = _n
  dropvars tag tag_expand list0
  egen tag = tag(cat123)
  expand 2 if tag==1, gen(tag_expand)  
  
  
  *-分类标题
  gen list0 = list  // 备份一下
  *replace list = "## " + linkCat if tag_expand==1   // 分类标题-含链接
  replace list = "## " + cat     if tag_expand==1   // 分类标题-仅文字
  *replace list = "  " + list if tag_expand==0
  gsort id_temp -tag -tag_exp
  *br list tag* cat* 
  
    local date = subinstr("`c(current_date)'"," ","",3)
    insobs 4, before(1)  //增加几行观察值，以便写大标题
    replace id = -9 in 1
    replace id = -8 in 2
    replace id = -7 in 3
    replace id = -6 in 4
    
    replace list = "## 连享会 - 推文列表" if id==-9
    replace list = "> &emsp;     " if id==-8
    replace list = "> &#x231A; Update: ``date'` &emsp;  &#x2B55; [**按时间顺序查看**](https://www.lianxh.cn/news/451e863542710.html)  " if id==-7
    replace list = "> &emsp;     " if id==-6
  
  *-输出 
	local date = subinstr("`c(current_date)'"," ","",3)
	*dis "`date'"
    export delimited list using "连享会主页_推文列表-分类_`date'.md", ///
           novar nolabel delimiter(tab) replace	 

    export delimited list using "$pp/连享会主页_推文列表-分类_`date'.md", ///
           novar nolabel delimiter(tab) replace	


  
  

	
 
  
  
  
  
  









/*

| catID | cat             | sort |
| ----- | --------------- | ---- |
| 44    | 专题课程        | 1    |
| 34    | 数据分享        | 2    |
| 31    | 论文写作        | 3    |
| 43    | Stata新命令     | 4    |
| 16    | Stata入门       | 8    |
| 17    | Stata教程       | 9    |
| 18    | 计量专题        | 10   |
| 35    | Stata资源       | 11   |
| 25    | Stata数据处理   | 12   |
| 24    | Stata绘图       | 13   |
| 26    | Stata程序       | 14   |
| 22    | 结果输出        | 17   |
| 32    | 回归分析        | 21   |
| 20    | 面板数据        | 22   |
| 38    | IV-GMM          | 24   |
| 39    | 倍分法DID       | 26   |
| 40    | 断点回归RDD     | 28   |
| 41    | PSM-Matching    | 30   |
| 42    | 合成控制法      | 32   |
| 19    | 内生性-因果推断 | 43   |
| 21    | 交乘项-调节     | 45   |
| 28    | 时间序列        | 47   |
| 29    | 空间计量        | 49   |
| 27    | Probit-Logit    | 51   |
| 36    | 文本分析-爬虫   | 61   |
| 37    | Python-R-Matlab | 63   |
| 45    | 风险管理        | 96   |
| 30    | Markdown        | 97   |
| 23    | 工具软件        | 98   |
| 33    | 其它            | 99   |




catID	cat	sort
44	专题课程	1
34	数据分享	2
31	论文写作	3
43	Stata新命令	4
16	Stata入门	8
17	Stata教程	9
18	计量专题	10
35	Stata资源	11
25	Stata数据处理	12
24	Stata绘图	13
26	Stata程序	14
22	结果输出	17
32	回归分析	21
20	面板数据	22
38	IV-GMM	24
39	倍分法DID	26
40	断点回归RDD	28
41	PSM-Matching	30
42	合成控制法	32
19	内生性-因果推断	43
21	交乘项-调节	45
28	时间序列	47
29	空间计量	49
27	Probit-Logit	51
36	文本分析-爬虫	61
37	Python-R-Matlab	63
45	风险管理	96
30	Markdown	97
23	工具软件	98
33	其它	99

*/
