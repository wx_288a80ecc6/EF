

cap program drop lianxh
program define lianxh
	version 15.0
	syntax [anything(name = class)] [,  Date Catlink NOTime NOTOP Saving(string)]
	preserve
	
	*- 对输入options进行预设
	if "`date'" == "" local date "0"
	else local date = subinstr("`c(current_date)'"," ","",3)
	
	if "`catlink'" == "" local catlink "0"
	else local catlink "1"
	
	if "`notime'" == "" local notime "0"
	else local notime "1"	

	if "`notop'" == "" local notop "0"
	else local notop "1"
	
	if "`saving'" ~= "" {
		capture mkdir `path'
	} 
	else {
		*local saving `c(pwd)'
		*disp "`path'"
		local saving "D:/vsCode/推文-lianxh.cn/OK-lianxh主页/主页推文列表"
	}
	
	
	if regexm("`path'", "(/|\\)$") local path = regexr("`path'", ".$", "")
	
	local address "https://www.lianxh.cn/blogs.html" 
	
	capture copy `"`address'"' "lianxh.txt", replace
	local times = 0
	while _rc ~= 0 {
		local times = `times' + 1
		sleep 1000
		cap copy `"`url'"' tempcsvfile.csv, replace
		if `times' > 10 {
			disp as error "Internet speeds is too low to get the data"
			exit 601
		}
	}
	
	capture {
	cap infix strL v 1-1000 using "lianxh.txt", clear
	cap save "lxh_temp.dta", replace

	use "lxh_temp.dta", replace
	*- 抽取原始链接
	local regex = `"(?<="><a href=")(.*)(?=")"' 
	* 以 「"><a href="」开头，「"」结尾的字符串 
	gen link = ustrregexs(0) if ustrregexm(v, `"`regex'"')
	replace link = "https://www.lianxh.cn" + link if link!=""
	
	*- 抽取标题
	local regex = `"(?<=html">)(.+)(?=</a></h3>)"'
	gen title = ustrregexs(0) if ustrregexm(v, `"`regex'"')
	
	
	*- 抽取分类标签
	local regex = `"(<span>)(\D.*)(?:</span>)"'  
	*-Note: 需要|<span>结果输出</span>|，而不需要 |<span>04/02</span>|, 所以，使用「\D」排除后者
	gen cat = ustrregexs(2) if ustrregexm(v, `"`regex'"')	
	drop if ustrregexm(cat, "(data\[item\])")
	*br if link!=""|cat!=""
	keep if link!=""|cat!=""
	replace cat = cat[_n+1] if cat==""
	keep if title!=""
	drop v
	drop if ustrregexm(title, "(data\[item\])")
	format title %-60s
	gen id = _n         // 推文发表时间序号
	*gsort cat title
	save "lxh_title.dta", replace 	
	
	*- 产生分类文件
	use "lxh_temp.dta", replace
	local regex = `"(?<=/blogs/)\d+(?=\.html)"'
	gen catID = ustrregexs(0) if ustrregexm(v, `"`regex'"')  // 分类编号
	
	keep if catID != ""
	local regex = `"(?<=\s>).+(?=</a>)"'
	gen cat = ustrregexs(0) if ustrregexm(v, `"`regex'"')  // 分类名称
	drop v 
	destring catID, replace 
	cap save "catID.dta", replace  // 临时保存文件，随后与主文件合并 
 
    

	*- 合并文件
	use "catID.dta", clear 
	merge 1:1 catID using "catSort.dta", nogen 
	merge 1:m cat   using "lxh_title.dta", nogen 
	gen list  =   "- [" + title + "](" + link + ")"
	gen website = `"{browse ""' + link +`"": ["' + title +`"]}"'
	*gen list2 = "  - [" + title + "](" + link + ")"
	save "Final_data.dta", replace 
	use "Final_data.dta",clear
	*- 前期数据处理完毕
	
	}
	
	
	*- 用户无输入时，只显示欢迎及资源导航页面
	if "`class'" == "" {
		  
	  dis _n in w _col(10) _dup(45) "="
	  dis    in w _col(20) _n _skip(25) "Hello, Stata!" _n
	  dis    in w _col(10) _dup(45) "=" _n 
	  
	  dis in w  "Stata官方：" ///
		  `"{browse "http://www.stata.com": [Stata.com] }"' ///
		  `"{browse "http://www.stata.com/support/faqs/":   [Stata-FAQ] }"' ///
		  `"{browse "https://blog.stata.com/":      [Stata-Blogs] }"' ///
		  `"{browse "http://www.stata.com/links/resources.html":   [Stata-资源链接] }"' 
	  dis in w  _col(12)  /// 
		  `"{browse "https://www.jianshu.com/p/8b48a32219b8": [Stata手册在线] }"' ///
		  `"{browse "https://gitee.com/arlionn/SJ":   [Stata Journal单篇在线浏览] }"'  _n
		  
	  dis in w  "Stata论坛：" ///
		  `"{browse "http://www.statalist.com": [Stata-list] }"'      ///
		  `"{browse "https://stackoverflow.com":  [Stack-Overflow] }"' ///
		  `"{browse "http://bbs.pinggu.org/": [经管之家-人大论坛] }"'  _n
	  
	  dis in w  "Stata资源：" /// 
		  `"{browse "http://www.jianshu.com/u/69a30474ef33": [Stata连享会-简书] }"' ///
		  `"{browse "https://www.zhihu.com/people/arlionn/":    [Stata连享会-知乎] }"'  ///
		  `"{browse "https://gitee.com/arlionn/Course":    [Stata连享会-码云] }"'
		  
	  dis in w  _col(12)  /// 
		  `"{browse "http://www.jianshu.com/p/f1c4b8762709": [Stata书单] }"' ///
		  `"{browse "http://www.jianshu.com/p/c723bb0dbf98":           [Stata资源汇总] }"' _n
		  
	  dis in w  "Stata在线课程：" ///
		  `"{browse "https://stats.idre.ucla.edu/stata/": [UCLA在线课程] }"' ///
		  `"{browse "http://www.princeton.edu/~otorres/Stata/":        [Princeton在线课程] }"' ///
		  `"{browse "http://wlm.userweb.mwn.de/Stata/":        [Internet Guide to Stata]}"'_n
		  
		  
	  dis in w  "Stata现场课程：" ///
		  `"{browse "http://www.peixun.net/view/307.html":[Stata初级班] }"'  ///
		  `"{browse "http://www.peixun.net/view/308.html":          [Stata高级班] }"' ///
		  `"{browse "https://gitee.com/arlionn/Course/blob/master/README.md":         [专题课程] }"' 
		  
	  dis in w  _col(15)  /// 		  
		  `"{browse "http://www.peixun.net/view/1135.html": [Stata学术论文班] }"' ///
		  `"{browse "http://i.youku.com/arlion":      [Stata优酷公开课] }"'  _n
		  
	  dis in w  "学术论文：" ///
		  `"{browse "https://scholar.google.com/": [Google学术]}"'  ///
		  `"{browse "http://scholar.cnki.net/":      [CNKI]}"' ///
		  `"{browse "http://scholar.chongbuluo.com/":        [学术搜索]}"'  
		  
	  dis in w  _col(11)  /// 	  
		  `"{browse "http://xueshu.baidu.com/": [百度学术]}"' ///
		  `"{browse "https://sci-hub.tw":        [SCI-HUB]}"' ///
		  `"{browse "http://www.jianshu.com/p/494e6feab565":     [Links/Tools] }"' _n
		  
	  dis in w  "Data/Prog：" ///	  		  
		  `"{browse "https://dataverse.harvard.edu/dataverse/harvard?q=stata": [Harvard dataverse]}"' ///
		  `"{browse "https://github.com/search?utf8=%E2%9C%93&q=stata&type=":      [Github]}"'	  _n  
		  
	  dis in w _col(2) _dup(5) "~" ///
		  as smcl `"{stata "cap github install arlionn/lxh, replace": 点击更新 }"' ///
		  _dup(5) "~"
		  }
		  
	*- 用户有输入情况，先根据关键词进行检索，不考虑options
	else {
	    // 加号情况，取交集
		if regexm("`class'", "\+") {
		    local times = 1
			local class_c = "`class'"
		    while regexm("`class_c'", "\+"){
				local times = `times' +1
				local class_c = regexr("`class_c'","\+"," ")
				local class_min = regexr("`class_c'","\+","")
			}
			tokenize `class_c'
			forvalues i = 1/`times'{
			cap gen index_`i' = regexm(title, `"``i''"')
			cap keep if index_`i' ==1
			}
			local n = _N
			forvalues j = 1/`n' {
				dis website[`j']
			}
			cap save `"`saving'/outcome"', replace
		}

			
		else{
		  // 空格情况，取并集
			gen code = 0
			foreach name in `class'{
				cap gen index_`name' = regexm(title,`"`name'"')
				cap replace code = code + index_`name'
			}
			cap keep if code != 0
			local n = _N
			forvalues j = 1/`n' {
				dis website[`j']
			}
			cap save `"`saving'/outcome"', replace
		} 
	
	
	*-options
	/*
  - Date	输出的 md 文档中是否包含日期，若加上就包含日期
  - Catlink	推文分类是否包含超链接
  - NOTime	不输出按日期分类的推文列表
  - NOTOP	不附加头尾信息
  - Saving() 存储路径，默认为 D:/vsCode/推文-lianxh.cn/OK-lianxh主页/主页推文列表"*/ 
    
    *- 定义生成的md文件命名的信息暂元
	if "`date'" == "0" local information "不含时间"
	else local information = "含时间" + "`date'"
	if "`linkCat'" == "0" local information = "`information'" + "-不含超链接"
	else local information = "`information'" + "-含超链接"  
	if "`notop'" == "0" local information = "`information'" + "-含头尾信息"
	else local information = "`information'" + "-无头尾信息"
	if "`notime'" == "0" local information = "`information'" + "-按时间顺序"
	else local information = "`information'" + "-按专题顺序"
	
	*-首先根据是否按照时间排序的大分类进行划分
	if "`notime'" == "0" {
	capture{
	use `"`saving'/outcome.dta"',clear
	*global pp "/Users/kangjunjie/Documents/VScode/推文-lianxh.cn/OK-lianxh主页/主页推文列表" 
	local date = subinstr("`c(current_date)'"," ","",3)
	insobs 4, before(1)  //增加一行观察值，以便写大标题
	replace id = -9 in 1
	replace id = -8 in 2
	replace id = -7 in 3
	replace id = -6 in 4
	cap if "`linkCat'" == "0" drop linkCat
	replace list = "## 连享会 - 推文列表 - `class'" if id==-9
	replace list = "> &emsp;     " if id==-8
	}
	if "`date'" != "0"{
		cap replace list = "> &#x231A; Update: ``date'` &emsp;  &#x2B55; [**按类别查看**](https://www.lianxh.cn/news/d4d5cd7220bc7.html)  " if id==-7
		cap replace list = "> &emsp;     " if id==-6
		sort id
		local date = subinstr("`c(current_date)'"," ","",3)
		dis "更新日期： `date'"
	}
	else{
		cap replace list = "> &emsp;  &#x2B55; [**按类别查看**](https://www.lianxh.cn/news/d4d5cd7220bc7.html)  " if id==-7
		cap replace list = "> &emsp;     " if id==-6
		sort id
		local date = subinstr("`c(current_date)'"," ","",3)
		dis "更新日期： `date'"
	}
	}

	else {
	cap{
		keep cat123 id list linkCat cat
		format list linkCat %-20s
		*- 不要链接，则drop掉
		cap if "`linkCat'" == "0" drop linkCat
		sort cat123 id 
		gen id_temp = _n
		dropvars tag tag_expand list0
		egen tag = tag(cat123)
		expand 2 if tag==1, gen(tag_expand)  
		
		
		*-分类标题
		gen list0 = list  // 备份一下
		*replace list = "## " + linkCat if tag_expand==1   // 分类标题-含链接
		replace list = "## " + cat     if tag_expand==1   // 分类标题-仅文字
		*replace list = "  " + list if tag_expand==0
		gsort id_temp -tag -tag_exp
		*br list tag* cat* 
		
		local date = subinstr("`c(current_date)'"," ","",3)
		insobs 4, before(1)  //增加几行观察值，以便写大标题
		replace id = -9 in 1
		replace id = -8 in 2
		replace id = -7 in 3
		replace id = -6 in 4

		replace list = "## 连享会 - 推文列表" if id==-9
		replace list = "> &emsp;     " if id==-8
		replace list = "> &#x231A; Update: ``date'` &emsp;  &#x2B55; [**按时间顺序查看**](https://www.lianxh.cn/news/451e863542710.html)  " if id==-7
		replace list = "> &emsp;     " if id==-6
    }
	
	}
	
	*- 如果不要表头，就直接丢掉第1-4行
	if "`notop'" == "1" drop in 1/4
	export delimited list using "`saving'/连享会主页_推文列表_`information'.md" , ///
		   novar nolabel delimiter(tab) replace		
}
restore	
end

	
