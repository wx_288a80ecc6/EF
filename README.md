## 实证金融 (Empirical Finance) 课程主页

### 课程报告选题
> `2020/10/30 17:42`   
大家好，我在 [课程报告 - 备选主题](https://gitee.com/arlionn/EF/wikis/readme.md?sort_id=3029983) 中添加了一些期末课程论文的选题，随后会陆续增加，大家可以先去看看，有意向的可以 readme.md 页面下留言区留言，认领选题，先到先得。     

ps, 【[其他选题](https://gitee.com/arlionn/EF/wikis/%E5%85%B6%E4%BB%96%E9%80%89%E9%A2%98.md?sort_id=3051252)】中的题目也可以选。`2020/10/30 19:25`  

**留言格式为：** 姓名-日期，标题 (从 [课程报告 - 备选主题](https://gitee.com/arlionn/EF/wikis/readme.md?sort_id=3029983) 页面中复制题目即可 )，


&emsp;


### 课件和 FAQs
- **课件下载：**
  - FTP 地址：ftp://ftp.lingnan.sysu.edu.cn/ &rarr; [2020_实证金融] 文件夹。
  - 账号 = 密码 = lianyjst
  - 请保护版权，未经本人同意，请勿散布于网络。
- **[FAQs](https://gitee.com/arlionn/PX/wikis/%E8%BF%9E%E7%8E%89%E5%90%9B-Stata%E5%AD%A6%E4%B9%A0%E5%92%8C%E5%90%AC%E8%AF%BE%E5%BB%BA%E8%AE%AE.md?sort_id=2662737)** 学习过程中的多数问题，都可以在这里找到答案。亦可访问 [连享会主页](https://www.lianxh.cn) 和 [连享会知乎](https://www.zhihu.com/people/arlionn/) 查阅相关推文。 

### &#x1F34E; 作业发布
- &#x1F449;  [【点击查看作业】](https://gitee.com/arlionn/EF/wikis/%E6%8F%90%E4%BA%A4%E6%96%B9%E5%BC%8F%E5%92%8C%E6%97%B6%E7%82%B9.md?sort_id=1628096) 
  - ${\color{red}{New}}$  HW03 已发布，提交截止时间：`2020/10/9 23:59` (本次作业可以两人组队完成)
  - HW02 已发布，提交截止时间：`2020/9/22 23:59` (本次作业可以两人组队完成)
  - HW01 已经发布，提交截止时间：`2020/9/12 23:59`
  

&emsp;

### &#x1F353; 交作业入口
> &#x1F449; **【[点击提交](https://workspace.jianguoyun.com/inbox/collect/9375d5be4dc24ac9a3f23bee9bff89a2/submit)】**，个人作业和期末课程报告均通过此处提交 


> Notes：
> - [1] 若需更新，只需将同名文件再次提交即可。
> - [2] 提交截止时间为 deadline 当日 23:59，逾期不候。
